Instructions
============

Behat: tests fonctionnels
-------------------------

Ces tests sont des tests génériques.
Cependant, ils nécessitent que le Drupal soit configuré de la sorte :

un rôle «administrator» et un rôle «editor»
installation en français, langue par défaut



Vieilles instructions qui ne seront plus valides quand le projet sera dockerisé

Copy behat/behat.yml.example to behat/behat.yml, and replace the placeholders with your values.

Run the tests:

    % ./behat

