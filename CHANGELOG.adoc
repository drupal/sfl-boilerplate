= Changelog

.0a115583cea98f085988cc6d3c89443feedb1118
24-03-2016

* The Drupalizer submodule has been renamed as *fabfile*, so it could be discovered as a Fabric module.

CAUTION: Be careful not to flush your config file during the following process.

To update:

 $ git submodule sync
 $ cp drupalizer/local_vars.py fabfile
 $ rm -rf drupalizer

* In non-interactive mode, the container IP is now managed in the local_vars.py config file.
It will be updated when running the task *docker.container_start*.
For it to work on Jenkins, you need to add manually the following line, if not already there:

 # Docker auto-added container IP

The current container IP will be added after this line. This is a workaround to avoid editing */etc/hosts*.

Once the line has been added, run:

 $ fab docker.container_stop
 $ fab docker.container_start

You should now see a new environment variable below the line you just added:

  $ env.container_ip = '172.17.0.3'

''''
