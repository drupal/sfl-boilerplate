<?php 

/**
 * DO NOT EDIT. Copy this file to a new file named aliases.drushrc.php.
 * Example file.
 *  
 * Drush aliases definition.
 */

$aliases['dk'] = array(
  'root' => '/var/www/drupal',
  'uri' => 'http://local.boilerplate.sfl',
  'remote-host' => 'local.boilerplate.sfl',
  'remote-user' => 'root',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
  ),
);

