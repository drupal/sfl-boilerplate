<?php

/**
 * @file
 * Local settings file.
 */

/**
 * Site & environment settings.
 */
$conf['site_name'] = 'SFL Boilerplate STAGING';
$conf['environment'] = 'staging';
