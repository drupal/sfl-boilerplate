<?php

/**
 * @file
 * Local settings file.
 */

/**
 * Site & environment settings.
 */
$conf['site_name'] = 'SFL Boilerplate LOCAL';
$conf['environment'] = 'local';
