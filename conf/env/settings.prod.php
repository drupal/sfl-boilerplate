<?php

/**
 * @file
 * Local settings file.
 */

/**
 * Site & environment settings.
 */
$conf['site_name'] = 'SFL Boilerplate';
$conf['environment'] = 'prod';
